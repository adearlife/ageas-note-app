const path = require('path');

const noteService = require('./services/notes_service');
const settingsService = require('./services/settings_service');

module.exports = (app) => {
    /////////////////
    // notes
    /////////////////

    app.get('/api/notes', noteService.getAllNotes);

    app.get('/api/notes/:id',  noteService.getIndividualNote);


    app.post('/api/notes', noteService.createNewNote);

    app.patch('/api/notes', noteService.updateNote);

    app.delete('/api/notes/:id', noteService.deleteNote);

    /////////////////
    // settings
    /////////////////

    app.post('/api/settings', settingsService.createSettings);

    app.get('/api/settings', settingsService.getSettings);

    //catch anything else and send it to react to deal with
    app.get('/**', function(req, res) {
        res.sendFile(path.join(__dirname, '../public', 'index.html'));
    });

};