// main starting point of app
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

app.use("/public", express.static(path.join(__dirname, '../public')));

const router = require('./router.js');

//db setup

// app setup
// anything that is passed into out app will be passed through these first
app.use(bodyParser.json({type: '*/*'}));

router(app);


// server setup
const port = 3333;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on:',port);

// exporting this for tests
module.exports = {
    app
};