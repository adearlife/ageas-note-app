const {Note} = require('../../models/notes');

const createUsers = () => {
    // before each test destroy all items in the Note test database and give it some fresh predictable data
    Note.destroy({where:{}}).then(() => {
        Note.create({
            title: 'test1 title',
            body: 'test1 body'
        });
        Note.create({
            title: 'test2 title',
            body: 'test2 body'
        });
    });
};

module.exports = {
    createUsers
};