const expect = require('expect');
const request = require('supertest');

const { app } = require('../index');
const { Note } = require('../models/notes');
const { createUsers } = require('./seed/seed');

beforeEach(createUsers);

// i would normully test every route however i just done have the time to get it all done before you require it
// i hope this is enough to show you that i have an understanding of testing.

describe('POST /api/notes', ()=> {
    it('should create a new note', (done) => {
        let post = {
            title:"created test title",
            body:"created test body"
        };
        request(app)
            .post('/api/notes')
            .send(post)
            .expect(200)
            .expect((res) => {
                expect(res.body.title).toBe(post.title);
            }).end((err, res) => {
            if(err) {
                return done();
            }
            Note.findAll().then((notes) => {
                expect(notes.length).toBe(3);
                expect(notes[0].title).toBe(post.title);
                done();
            }).catch((e) => {
                done(e);
            });
        });

    });

    it('should not create todo without a title', (done) => {
        let post = {
            body:"created test body"
        };
        request(app)
            .post('/api/notes')
            .send(post)
            .expect(400)
            .expect((res) => {
                expect(res.body.err).toBe('A title is required');
            }).end((err, res) => {
            if(err) {
                return done();
            }
            Note.findAll().then((notes) => {
                expect(notes.length).toBe(2);
                done();
            }).catch((e) => {
                done(e);
            });
        });
    })

});

