const Sequelize = require('sequelize');
const sqlite3 = require('sqlite3');
const env = process.env.NODE_ENV || 'development';

// sets storage location depending on env
const storage = env === 'test' ? 'server/database/test_database.db' : 'server/database/database.db';
// creates database if it does not exist
const db = new Sequelize('note-app', null, null, {
    host: 'localhost',
    dialect: 'sqlite',
    storage
});

// creates Note model
const Note = db.define('note', {
    title: {
        type: Sequelize.STRING
    },
    body: {
        type: Sequelize.STRING
    }
});
// creates table if it does not exist
Note.sync();

// exports the model for use in app
module.exports = {
    Note
};
