const fs = require('fs');

// if i wanted to do something like this in production i would use sql seeings as it is already set up but this is using the fs as requested.

function createSettings(req, res) {
    if (!req.body.background) {
        return res.status(400).send({err: 'A color is required'});
    }

    fs.writeFileSync('settings-data.json', JSON.stringify({background: req.body.background}));

    res.send(req.body);

}

function getSettings(req, res) {
    try {
        let settingsString = fs.readFileSync('settings-data.json');
        return res.send(JSON.parse(settingsString));
    } catch (e) {
        return {};
    }
}

module.exports = {
    createSettings,
    getSettings
};