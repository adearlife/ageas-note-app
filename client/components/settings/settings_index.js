import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import { fetchSettings ,createSettings } from '../../actions/settings_actions';
import '../../assets/styles/notes.scss';

class NoteForm extends Component {

    componentDidMount() {
            this.props.fetchSettings();
    }

    renderTextField(field) {
        const {meta: {touched, error}} = field;
        return (
            <div>
                {field.label ? <label>{field.label}</label> : ''}
                <input
                    type="text"
                    placeholder={field.placeholder}
                    {...field.input}
                />
                <span className="text-red"> {touched ? error : ''} </span>
            </div>
        );
    }


    submitForm(values) {
        this.props.createSettings(values);
    }


    render() {
        const {handleSubmit} = this.props;
        return (
            <div className="notes_app_form">
                <h1> Change your settings </h1>
                <form onSubmit={handleSubmit(this.submitForm.bind(this))}>
                    <Field
                        label="Top navbar buttons colour (please use a valid hex code)"
                        placeholder="Buttons colour"
                        name="background"
                        component={this.renderTextField}
                    />
                    <button type="submit">Submit</button>
                    <Link to="/"><i className="fa fa-arrow-left" aria-hidden="true"></i>Back </Link>
                </form>

            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.background) {
        errors.background = "Please enter color";
    }

    return errors;

}

function mapStateToProps(state) {
    if (state.settings.background) {
        return {initialValues: state.settings};
    }
    return {}

}

export default connect(mapStateToProps, {fetchSettings, createSettings})(reduxForm({
    validate,
    form: 'newSettings'
})(NoteForm))
