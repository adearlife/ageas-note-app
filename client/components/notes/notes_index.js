import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import _ from 'lodash';

import {fetchNotes} from '../../actions/notes_actions';
import '../../assets/styles/notes.scss';

class NotesIndex extends Component {
    componentDidMount() {
        this.props.fetchNotes();
    }

    limitChars(after, string, length) {
        if (string) {
            if (string.length < parseInt(length)) {
                return `${string}`;
            }
            return `${string.substring(0, parseInt(length) - 1)}${after}`;
        }
        return '';
    }

    renderNotes() {
        return _.map(this.props.notes, note => {
            return (
                <div key={note.id} className="single_note_container">
                    <div className="single_note">

                        <div className="main_detail_area">
                            <Link to={`/${note.id}`}><h3>{this.limitChars('...', note.title, 50)}</h3></Link>
                            <hr />
                            <p>{this.limitChars('...', note.body, 200)}</p>
                        </div>
                        <hr />
                        <Link to={`/${note.id}`}>View note </Link>
                    </div>
                </div>
            );
        });
    }

    render() {

        if (_.isEmpty(this.props.notes)) {
            return (
                <div className="notes_list">
                    <h1> All notes </h1>
                    <div className="no_notes_to_show">
                        <h2> It appears you have no notes. <Link to={`/new-note`}>Create your first note here </Link> </h2>
                    </div>
                </div>
            );
        }

        return (
            <div className="notes_list">
                <h1> All notes </h1>
                {this.renderNotes()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        notes: state.notes
    };
}

export default connect(mapStateToProps, {fetchNotes})(NotesIndex);
