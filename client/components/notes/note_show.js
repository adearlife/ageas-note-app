import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import moment from 'moment';

import {fetchNote, deleteNote} from '../../actions/notes_actions';
import '../../assets/styles/notes.scss';


class NoteShow extends Component {
    componentDidMount() {
        const {id} = this.props.match.params;
        this.props.fetchNote(id);
    }

    deleteThisNote() {
        const {id} = this.props.match.params;
        this.props.deleteNote(id, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const {note} = this.props;

        if (!note) {
            return <div>loading...</div>
        }

        return (
            <div className="note_single">
                <div className="note_contents">
                    <h1>{note.title}</h1>
                    <hr/>
                    <p>Last updated: { moment(note.updatedAt).fromNow() } , Created: {moment(note.createdAt).format("MMM Do YYYY") }</p>
                    <p>{ note.body }</p>
                </div>
                <div className="actions_section">
                    <Link to={`/edit-note/${note.id}`}><i className="fa fa-pencil" aria-hidden="true"></i> edit note </Link>
                    <Link to="/"><i className="fa fa-arrow-left" aria-hidden="true"></i>Back </Link>
                    <button
                        className="btn btn-danger pull-xs-right"
                        onClick={this.deleteThisNote.bind(this)}
                    >
                        Delete Post
                    </button>
                    <div className="clear_both"></div>
                </div>
            </div>


        );
    }
}

function mapStateToProps({notes}, ownProps) {
    return {note: notes[ownProps.match.params.id]};
}

export default connect(mapStateToProps, {fetchNote, deleteNote})(NoteShow);