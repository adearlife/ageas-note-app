import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {fetchNote} from '../../actions/notes_actions';
import '../../assets/styles/notes.scss';

class NoteForm extends Component {

    componentDidMount() {
        if (this.props.noteId) {
            this.props.fetchNote(this.props.noteId.id);
        }
    }

    renderTextField(field) {
        const {meta: {touched, error}} = field;
        return (
            <div>
                <input
                    type="text"
                    placeholder={field.placeholder}
                    {...field.input}
                />
                <span className="text-red"> {touched ? error : ''} </span>
            </div>
        );
    }

    renderTextarea(field) {
        const {meta: {touched, error}} = field;
        return (
            <div>
                <textarea
                    {...field.input}
                    rows="12"
                    placeholder={field.placeholder}
                />
                <span className="text-red"> {touched ? error : ''} </span>
            </div>
        );
    }

    renderBackButton() {
        if (this.props.noteId) {
           return <Link to={`/${this.props.noteId.id}`}><i className="fa fa-arrow-left" aria-hidden="true"></i>Back </Link>
        }
        return <Link to="/"><i className="fa fa-arrow-left" aria-hidden="true"></i>Back </Link>
    }



    render() {
        const {handleSubmit} = this.props;
        return (
            <div className="notes_app_form">
                <h1> {this.props.title} </h1>
                <form onSubmit={handleSubmit(this.props.submitForm.bind(this))}>
                    <Field
                        placeholder="Note title"
                        name="title"
                        component={this.renderTextField}
                    />

                    <Field
                        placeholder="Note Body"
                        name="body"
                        component={this.renderTextarea}
                    />
                    <button type="submit">{this.props.title}</button>
                    {this.renderBackButton()}
                </form>

            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.title) {
        errors.title = "Please enter a title";
    }
    if (!values.body) {
        errors.body = "please enter a body";
    }

    return errors;

}

function mapStateToProps(state, ownProps) {
    if (ownProps.noteId) {
        return {initialValues: state.notes[ownProps.noteId.id]};
    }
    return {};

}

export default connect(mapStateToProps, {fetchNote})(reduxForm({
    validate,
    form: 'newNote'
})(NoteForm))
