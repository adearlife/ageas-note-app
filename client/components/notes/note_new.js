import React, { Component } from 'react';
import { connect } from 'react-redux';

import NoteForm from './form';
import { createNote } from '../../actions/notes_actions'
import '../../assets/styles/notes.scss';

class NoteNew extends Component {

    submitForm(values) {
        this.props.createNote(values , (id) => {
            this.props.noteHistory.push(`/${id}`);
        });
    }

    render() {
        return (
            <div>
                <NoteForm submitForm={this.submitForm} createNote={this.props.createNote} noteHistory={this.props.history} title="Create Note"  />
            </div>
        );
    }
}

export default connect(null, { createNote })(NoteNew);