import React, { Component } from 'react';
import { connect } from 'react-redux';

import NoteForm from './form';
import { updateNote } from '../../actions/notes_actions'
import '../../assets/styles/notes.scss';

class NoteEdit extends Component {

    submitForm(values) {
        const id = this.props.noteId.id;
        this.props.updateNote(values , () => {
            this.props.noteHistory.push(`/${id}`);
        });
    }

    render() {
        return (
            <div>
                <NoteForm noteId={this.props.match.params} submitForm={this.submitForm} updateNote={this.props.updateNote} noteHistory={this.props.history} title="Update Note"/>
            </div>
        );
    }
}

export default connect(null, { updateNote })(NoteEdit);