import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {fetchSettings} from '../actions/settings_actions';
import '../assets/styles/notes.scss';

class Header extends Component {

    componentDidMount() {
        this.props.fetchSettings();
    }
    // componentDidUpdate() {
    //     this.props.fetchSettings();
    // }

    backgroundColor() {
        if(this.props.settings) {
            return {'backgroundColor': this.props.settings.background};
        }
        return {'backgroundColor':'#59afe1'};
    }

    render() {
        return (
            <div >
                <div className="nav_bar">
                    <Link style={this.backgroundColor()} to="/"><i className="fa fa-home" aria-hidden="true"></i>Notes </Link>
                    <Link style={this.backgroundColor()} to="/new-note"><i className="fa fa-plus" aria-hidden="true"></i> New Note </Link>
                    <Link style={this.backgroundColor()} to="/settings"><i className="fa fa-cog" aria-hidden="true"></i> Settings </Link>
                </div>
                <div className="clear_both">
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings
    };
}

export default connect(mapStateToProps, {fetchSettings})(Header);
