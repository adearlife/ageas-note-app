import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';

import NotesReducer from './notes_reducer';
import SettingsReducer from './settings_reducer';


const rootReducer = combineReducers({
    form,
    notes: NotesReducer,
    settings: SettingsReducer
});

export default rootReducer;
