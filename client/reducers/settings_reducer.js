import { FETCH_SETTINGS, CREATE_SETTINGS } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {

        case FETCH_SETTINGS:
            return action.payload.data;
        case CREATE_SETTINGS:
            return action.payload.data;
        default:
            return state
    }
}