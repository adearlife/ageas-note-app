import _ from 'lodash';

import { FETCH_NOTES, FETCH_NOTE, DELETE_NOTE } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case FETCH_NOTES:
            // im using lodashes map keys here as it takes the onbject and and creates a map from it using the id as a key, this means when i am fetching or patching an individual note rather than having to fetch the whole list again i can use this to change the instance with that id.
            return _.mapKeys(action.payload.data , 'id');
        case FETCH_NOTE:
            // so because of using lodash i can now just add the one note onto the state list
            return {...state , [action.payload.data.id] : action.payload.data};
        case DELETE_NOTE:
            return _.omit(state, action.payload);
        default:
            return state
    }
}