import axios from 'axios';

import { FETCH_SETTINGS, CREATE_SETTINGS } from './types';

const API_URL = `${location.protocol}/api/settings/`;

function fetchSettings() {
    const request = axios.get(API_URL);
    return {
        type: FETCH_SETTINGS,
        payload: request
    };
}

function createSettings(values) {

    const request = axios.post(API_URL, values);

    return {
        type: CREATE_SETTINGS,
        payload: request
    };


}


module.exports = {
    fetchSettings,
    createSettings
};