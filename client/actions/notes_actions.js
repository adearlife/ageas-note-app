import axios from 'axios';

import { FETCH_NOTES, FETCH_NOTE, DELETE_NOTE, UPDATE_NOTE, CREATE_NOTE } from './types';

const API_URL = `${location.protocol}/api/notes/`;

function fetchNotes() {
    const request = axios.get(API_URL);

    return {
        type: FETCH_NOTES,
        payload: request
    };
}

function fetchNote(id) {
    const request = axios.get(`${API_URL}${id}`);

    return {
        type: FETCH_NOTE,
        payload: request
    };
}

function deleteNote(id, callback) {
    const request = axios.delete(`${API_URL}${id}`).then(() => {
        callback();
    });

    return {
        type: DELETE_NOTE,
        payload: request
    };
}

function updateNote(values, callback) {
    const request = axios.patch(API_URL, values).then(() => {
        callback();
    });

    return {
        type: UPDATE_NOTE,
        payload: request
    }
}

function createNote(values, callback) {
    const request = axios.post(API_URL, values).then((response) => {
        callback(response.data.id);
    });

    return {
        type: CREATE_NOTE,
        payload: request
    }
}

module.exports = {
    fetchNotes,
    fetchNote,
    deleteNote,
    updateNote,
    createNote
};