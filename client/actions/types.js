export const FETCH_NOTES = 'fetch_notes';
export const FETCH_NOTE = 'fetch_note';
export const DELETE_NOTE = 'delete_note';
export const UPDATE_NOTE = 'update_note';
export const CREATE_NOTE = 'create_note';
export const FETCH_SETTINGS = 'fetch_settings';
export const CREATE_SETTINGS = 'create_settings';
