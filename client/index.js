import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';
import NotesIndex from './components/notes/notes_index';
import NoteShow from './components/notes/note_show';
import NoteNew from './components/notes/note_new';
import NoteEdit from './components/notes/note_edit';
import Settings from './components/settings/settings_index';
import Header from './components/header';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div>
                <div className="container">
                <Header/>
                <Switch>
                    <Route path="/edit-note/:id" component={NoteEdit}/>
                    <Route path="/new-note" component={NoteNew}/>
                    <Route path="/settings" component={Settings}/>
                    <Route path="/:id" component={NoteShow}/>
                    <Route path="/" component={NotesIndex}/>
                </Switch>
                </div>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('app'));