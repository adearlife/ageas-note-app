# Andrew Dear's Ageas note app #

## To start app: ##
git clone https://bitbucket.org/adearlife/ageas-note-app.git

cd ageas-note-app/

npm install, 

npm run build,

npm start

## If you wish to run tests: ##

npm run test

## Rundown of my process: ##

.Create a boiler plate for the project where i set up node and react and ensure that they where working.

.Set up scss compiler with webpack so that i could use scss within the project and import it into react.

.Set up the database connection with Sequelize (I used Sequelize because i had heard how good it was so wanted to take the opportunity to learn how to use it).

.After first server end point then wrote its tests (sorry i did not test all routes as i fear i will not have the time).

.Completed the rest of the server end points.

.Completed the get request react components.

.Had to refactor the delete api end point as axios does not allow you to send information on a delete request so i placed the id in the url request instead.

.Completed patch and create react components.

.Implemented styling and ui changes. 

.Inserted settings to change the colour of the menu buttons (create to show understanding of the node fs system)

I have done all that i could get done in the spare time that i had this week. There are still lots of little refinements i would have like to have made but i hope this is enough.

if you have any issues please contact me any time. mob: 07455241441 , email:andrewdear2@gmail.com